resource "google_sql_database_instance" "replicas" {
  count = var.number_replicas

  name                 = "${google_sql_database_instance.master.name}-replicas${count.index}"
  project              = var.project
  database_version     = var.database_version
  deletion_protection  = false
  region               = var.region
  master_instance_name = google_sql_database_instance.master.name

  settings {
    activation_policy = "ALWAYS"
    tier              = var.tier
    disk_autoresize   = true
    disk_type         = var.disk_type

    ip_configuration {
      ipv4_enabled    = lookup(var.ip_configuration, "ipv4_enabled", false)
      private_network = lookup(var.ip_configuration, "private_network", null)
      require_ssl     = lookup(var.ip_configuration, "require_ssl", false)

      dynamic "authorized_networks" {
        for_each = var.authorized_networks

        content {
          value = authorized_networks.value
        }
      }
    }
  }

  replica_configuration {
    failover_target         = lookup(var.replica_configuration, "failover_target", true)
    master_heartbeat_period = lookup(var.replica_configuration, "master_heartbeat_period", 500)
  }
}