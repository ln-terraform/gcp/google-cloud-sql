output "name" {
  value = google_sql_database_instance.master.name
}

output "id" {
  value = google_sql_database_instance.master.id
}

output "selflink" {
  value = google_sql_database_instance.master.self_link
}