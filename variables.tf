variable "project" {
  type = string
}

variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "tier" {
  type = string
}

variable "root_password" {
  type = string
}

variable "master_instance_name" {
  type    = string
  default = "master"
}

variable "availability_type" {
  type    = string
  default = "REGIONAL"
}

variable "disk_type" {
  type    = string
  default = "PD_SSD"
}

variable "deletion_protection" {
  type    = bool
  default = true
}

variable "number_replicas" {
  type    = number
  default = 1
}

variable "database_version" {
  type    = string
  default = "POSTGRES_13"
}

variable "backup_configuration" {
  type = map(string)
  default = {
    enabled                        = true
    binary_log_enabled             = false
    start_time                     = "00:00"
    transaction_log_retention_days = 7
  }
}

variable "ip_configuration" {
  type = map(string)
  default = {
    ipv4_enabled = true
    require_ssl  = false
  }
}

variable "maintenance_window" {
  type = object({
    day  = number
    hour = number
  })
  default = {
    day  = 7
    hour = 0
  }
}

variable "replica_configuration" {
  type = map(string)
  default = {
    failover_target         = false
    master_heartbeat_period = 500
  }
}

variable "databases" {
  type    = list(string)
  default = []
}

variable "authorized_networks" {
  type    = list(string)
  default = []
}