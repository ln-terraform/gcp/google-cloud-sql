all: format plan

format:
	terraform fmt

plan: format
	terraform plan -var-file=example.tfvars.json

test: format
	terraform plan -var-file=example.tfvars.json

apply: format
	terraform apply -var-file=example.tfvars.json

destroy: format
	terraform destroy -var-file=example.tfvars.json