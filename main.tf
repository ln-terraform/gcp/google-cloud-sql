resource "random_id" "db_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "master" {
  name                = "${var.name}-${random_id.db_suffix.dec}-master"
  project             = var.project
  database_version    = var.database_version
  deletion_protection = false
  region              = var.region

  settings {
    activation_policy = "ALWAYS"
    tier              = var.tier
    availability_type = var.availability_type
    disk_autoresize   = true
    disk_type         = var.disk_type

    backup_configuration {
      enabled                        = lookup(var.backup_configuration, "enabled", true)
      binary_log_enabled             = lookup(var.backup_configuration, "binary_log_enabled", false)
      start_time                     = lookup(var.backup_configuration, "start_time", true)
      transaction_log_retention_days = lookup(var.backup_configuration, "transaction_log_retension_days", 7)
    }

    ip_configuration {
      ipv4_enabled    = lookup(var.ip_configuration, "ipv4_enabled", false)
      private_network = lookup(var.ip_configuration, "private_network", null)
      require_ssl     = lookup(var.ip_configuration, "require_ssl", false)

      dynamic "authorized_networks" {
        for_each = var.authorized_networks

        content {
          value = authorized_networks.value
        }
      }
    }

    maintenance_window {
      day  = var.maintenance_window.day
      hour = var.maintenance_window.hour
    }
  }

  lifecycle {
    ignore_changes = [
      settings[0].maintenance_window,
    ]
  }
}

resource "google_sql_user" "users" {
  name     = "root"
  project  = var.project
  instance = google_sql_database_instance.master.name
  type     = "BUILT_IN"
  password = var.root_password
  deletion_policy = "ABANDON"

  lifecycle {
    ignore_changes = [
      type,
    ]
  }
}

resource "google_sql_database" "db" {
  for_each = toset(var.databases)

  name     = each.key
  project  = var.project
  instance = google_sql_database_instance.master.name
}